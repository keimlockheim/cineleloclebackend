<?php
    use MaicAnthoine\cinelelocle\Models\Film;
    use MaicAnthoine\cinelelocle\Models\Seance;


    Route::get('api/seances', 'MaicAnthoine\Cinelelocle\Controllers\SeanceController@showSeances');
    Route::get('api/seance/{id}', 'MaicAnthoine\Cinelelocle\Controllers\SeanceController@getSeanceInfo');
    Route::get('api/films', 'MaicAnthoine\Cinelelocle\Controllers\FilmController@showFilms');
    Route::get('api/film/{id}', 'MaicAnthoine\Cinelelocle\Controllers\FilmController@getFilmInfo');

    //Donne un lien d'accès au frontend depuis le backend
    Route::get('/backend/billetterie', function () {
      return redirect('http://localhost:8080');
    });
 ?>
