<?php namespace MaicAnthoine\Cinelelocle\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

use Illuminate\Support\Facades\DB;
use MaicAnthoine\cinelelocle\Models\Film;

class FilmController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('MaicAnthoine.Cinelelocle', 'film');
    }

    //
    // Methods api
    //

    /**
     * Affiche tous les films.
     *
     * @return Response
     */
    public function showFilms()
    {
        $films = Film::all();
        if($films){
          foreach ($films as $key => $film) {
            $listFilm[$key] = $this->getFilmInfo($film->id);

          }
        }
        return $listFilm;
    }

    //
    //Permet d'obtenir les informations d'un film à partir de son id
    public function getFilmInfo($id)
    {
        $film = Film::where('id', '=', $id)->first();

        if ($film)
        {
            $filmInfo   = array(
                'error' => false,
                'titre' => $film->titre,
                'filmID' => $film->id,
                'ageLegal' => $film->age_legal,
                'ageConseille' => $film->age_conseille,
                'dureeEnMinute' => $film->duree_en_minute,
                'resume' => $film->resume,
                'videos' => $film->videos,
                'poster' => $film->poster
            );

            $filmGenres = json_decode($film->genres);
            foreach ($filmGenres as $genre)
            {
                $genreList[] = array(
                    "nom" => $genre->nom,

                );
            }
            $genreList = array(
                'Genres' => $genreList
            );
            return response()->json(array_merge($filmInfo, $genreList));
        }
        else
        {
            return response()->json(array(
                'error' => true,
                'description' => "Nous n'avons pas trouvé de film avec le numéro d'id suivant: "  . $id
            ));
        }
    }
}
