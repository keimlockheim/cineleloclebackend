<?php namespace MaicAnthoine\Cinelelocle\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

use Illuminate\Support\Facades\DB;
use MaicAnthoine\cinelelocle\Models\Seance;

class SeanceController extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('MaicAnthoine.Cinelelocle', 'seance');
    }


    //
    // Methods api
    //

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function showSeances()
    {

      $seances = Seance::all();
      if($seances){
        foreach ($seances as $key => $seance) {
          $listSeance[$key] = $this->getSeanceInfo($seance->id);

        }
      }

      return $listSeance;
    }

    //Permet d'obtenir les infos d'une séance à partir de son id
    public function getSeanceInfo($id)
    {
        $seance = Seance::where('id', '=', $id)->first();

        if ($seance)
        {
            $seanceInfo   = array(
                'error' => false,
                'dateHeure' => $seance->date_heure,
                'filmID' => $seance->film_id,
                'seanceID' => $seance->id,
                'film' => $seance->film

            );


            $filmGenres = json_decode($seance->film->genres);

            foreach ($filmGenres as $genre)
            {

                $genreList[] = array(
                    "nom" => $genre->nom,

                );
            }
            $genreList = array(
                'genres' => $genreList
            );

            $filmPosters = json_decode($seance->film->poster);

            foreach ($filmPosters as $poster)
            {

                $posterList[] = array(
                    "path" => $poster->path,

                );
            }
            $posterList = array(
                'posters' => $posterList
            );
            return response()->json(array_merge($seanceInfo,$genreList,$posterList));
        }
        else
        {
            return response()->json(array(
                'error' => true,
                'description' => "Nous n'avons pas trouvé de séance avec le numéro d'id suivant: "  . $id
            ));
        }
    }
}
