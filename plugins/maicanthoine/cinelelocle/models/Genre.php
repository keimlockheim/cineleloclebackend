<?php namespace MaicAnthoine\Cinelelocle\Models;

use Model;

/**
 * Model
 */
class Genre extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'maicanthoine_cinelelocle_genres';


    /*
    * Relations Laravel
    */
    public function Films(){

      return $this-> belongsToMany('maicanthoine\cinelelocle\models\Film' , 'maicanthoine_cinelelocle_film_genre');
    }
}
