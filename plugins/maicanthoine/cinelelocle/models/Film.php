<?php namespace MaicAnthoine\Cinelelocle\Models;

use Model;

/**
 * Model
 */
class Film extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'maicanthoine_cinelelocle_films';


    //La liste de vidéo devient jasonable dans october cms
    protected $jsonable = ["videos"];


    /**
    * Relations OCTOBER CMS
    **/
    public $attachMany = [
        'poster' => 'System\Models\File'
    ];

    public $belongsToMany = [
        'genres' => [
          'maicanthoine\cinelelocle\models\Genre',
          'table' => 'maicanthoine_cinelelocle_film_genre',
          'order' => 'nom'
        ]
    ];

    /*
    * Relations Laravel
    */
    public function Genres(){
      return $this->belongsToMany('maicanthoine\cinelelocle\models\Genre' , 'maicanthoine_cinelelocle_film_genre');
    }

    public function Seance(){
      return $this->belongsToOne('maicanthoine\cinelelocle\models\Seance' , 'maicanthoine_cinelelocle_seances');
    }

}
