<?php namespace MaicAnthoine\Cinelelocle\Models;

use Model;

/**
 * Model
 */
class Seance extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'maicanthoine_cinelelocle_seances';


    //
    //   Relations OCTOBER CMS
    //
    public $belongsTo = [
        'film' => ['maicanthoine\cinelelocle\models\Film']
    ];

    // /*
    // * Relations Laravel
    // */
    // public function Film(){
    //   return $this->belongsTo('maicanthoine\cinelelocle\models\Film' , 'id');
    // }


}
