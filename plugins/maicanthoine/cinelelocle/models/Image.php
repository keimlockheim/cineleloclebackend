<?php namespace MaicAnthoine\Cinelelocle\Models;

use Model;

/**
 * Model
 */
class Image extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'maicanthoine_cinelelocle_images';

    public function film()
    {
        return $this->hasOne('Film', 'id_film');
    }
}
