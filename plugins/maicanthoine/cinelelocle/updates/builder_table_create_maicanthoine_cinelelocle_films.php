<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMaicanthoineCinelelocleFilms extends Migration
{
    public function up()
    {
        Schema::create('maicanthoine_cinelelocle_films', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titre', 50);
      		  $table->smallInteger('dureeEnMinute');
      		  $table->smallInteger('ageLegal');
        		$table->smallInteger('ageConseille');
        		$table->string('resume', 250);
        		$table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('maicanthoine_cinelelocle_films');
    }
}
