<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class BuilderTableCreateMaicanthoineCinelelocleForeignKeys extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_seances', function(Blueprint $table) {
			$table->foreign('id_film')->references('id')->on('maicanthoine_cinelelocle_films')
						->onDelete('cascade')
						->onUpdate('cascade');
  		});
  		Schema::table('maicanthoine_cinelelocle_videos', function(Blueprint $table) {
  			$table->foreign('id_film')->references('id')->on('maicanthoine_cinelelocle_films')
  						->onDelete('cascade')
  						->onUpdate('cascade');
  		});
  		Schema::table('maicanthoine_cinelelocle_images', function(Blueprint $table) {
  			$table->foreign('id_film')->references('id')->on('maicanthoine_cinelelocle_films')
  						->onDelete('cascade')
  						->onUpdate('cascade');
  		});
  		Schema::table('maicanthoine_cinelelocle_film_genre', function(Blueprint $table) {
  			$table->foreign('id_film')->references('id')->on('maicanthoine_cinelelocle_films')
  						->onDelete('cascade')
  						->onUpdate('cascade');
  		});
  		Schema::table('maicanthoine_cinelelocle_film_genre', function(Blueprint $table) {
  			$table->foreign('id_genre')->references('id')->on('maicanthoine_cinelelocle_genres')
  						->onDelete('cascade')
  						->onUpdate('cascade');
  		});
    }

    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_seances', function(Blueprint $table) {
			    $table->dropForeign('maicanthoine_cinelelocle_seances_id_film_foreign');
    		});
    		Schema::table('maicanthoine_cinelelocle_videos', function(Blueprint $table) {
    			$table->dropForeign('maicanthoine_cinelelocle_videos_id_film_foreign');
    		});
    		Schema::table('maicanthoine_cinelelocle_images', function(Blueprint $table) {
    			$table->dropForeign('maicanthoine_cinelelocle_images_id_film_foreign');
    		});
    		Schema::table('maicanthoine_cinelelocle_film_genre', function(Blueprint $table) {
    			$table->dropForeign('maicanthoine_cinelelocle_film_genre_id_film_foreign');
    		});
    		Schema::table('maicanthoine_cinelelocle_film_genre', function(Blueprint $table) {
    			$table->dropForeign('maicanthoine_cinelelocle_film_genre_id_genre_foreign');
    		});
    }
}
