<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleFilms extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->smallInteger('duree_en_minute');
            $table->smallInteger('lage_legal');
            $table->smallInteger('age_conseille');
            $table->string('slug');
            $table->dropColumn('dureeenminute');
            $table->dropColumn('agelegal');
            $table->dropColumn('ageconseille');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->dropColumn('duree_en_minute');
            $table->dropColumn('lage_legal');
            $table->dropColumn('age_conseille');
            $table->dropColumn('slug');
            $table->smallInteger('dureeEnMinute');
            $table->smallInteger('ageLegal');
            $table->smallInteger('ageConseille');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
