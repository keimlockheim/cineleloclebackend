<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMaicanthoineCinelelocleGenres extends Migration
{
    public function up()
    {
        Schema::create('maicanthoine_cinelelocle_genres', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('maicanthoine_cinelelocle_genres');
    }
}