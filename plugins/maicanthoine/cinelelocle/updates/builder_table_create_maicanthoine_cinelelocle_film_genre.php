<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMaicanthoineCinelelocleFilmGenre extends Migration
{
    public function up()
    {
        Schema::create('maicanthoine_cinelelocle_film_genre', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id_film')->unsigned();
            $table->integer('id_genre')->unsigned();
            $table->primary(['id_film','id_genre']);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('maicanthoine_cinelelocle_film_genre');
    }
}
