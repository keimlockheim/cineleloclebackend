<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleSeances extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_seances', function($table)
        {
            $table->string('slug')->nullable();
            $table->renameColumn('dateheure', 'date_heure');
            $table->renameColumn('nbplacerestant', 'nb_place_restant');
            $table->renameColumn('id_film', 'film_id');
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_seances', function($table)
        {
            $table->dropColumn('slug');
            $table->renameColumn('date_heure', 'dateheure');
            $table->renameColumn('nb_place_restant', 'nbplacerestant');
            $table->renameColumn('film_id', 'id_film');
        });
    }
}
