<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleGenres extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_genres', function($table)
        {
            $table->string('nom');
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_genres', function($table)
        {
            $table->dropColumn('nom');
            $table->dropColumn('slug');
        });
    }
}
