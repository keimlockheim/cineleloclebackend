<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMaicanthoineCinelelocleVideos extends Migration
{
    public function up()
    {
        Schema::create('maicanthoine_cinelelocle_videos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('nom');
            $table->string('url');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('maicanthoine_cinelelocle_videos');
    }
}
