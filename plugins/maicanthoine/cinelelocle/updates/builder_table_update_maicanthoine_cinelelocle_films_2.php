<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleFilms2 extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->text('resume')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->string('resume', 250)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
