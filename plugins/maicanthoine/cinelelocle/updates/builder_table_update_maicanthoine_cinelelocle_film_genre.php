<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleFilmGenre extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_film_genre', function($table)
        {
            $table->dropPrimary(['id_film','id_genre']);
            $table->integer('film_id')->unsigned();
            $table->integer('genre_id')->unsigned();
            $table->dropColumn('id_film');
            $table->dropColumn('id_genre');
            $table->primary(['film_id','genre_id']);
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_film_genre', function($table)
        {
            $table->dropPrimary(['film_id','genre_id']);
            $table->dropColumn('film_id');
            $table->dropColumn('genre_id');
            $table->integer('id_film')->unsigned();
            $table->integer('id_genre')->unsigned();
            $table->primary(['id_film','id_genre']);
        });
    }
}
