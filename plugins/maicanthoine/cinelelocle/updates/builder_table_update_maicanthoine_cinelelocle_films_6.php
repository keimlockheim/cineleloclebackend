<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleFilms6 extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->string('slug', 191)->nullable()->change();
            $table->text('videos')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->string('slug', 191)->nullable(false)->change();
            $table->text('videos')->nullable(false)->change();
        });
    }
}
