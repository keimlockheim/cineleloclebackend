<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMaicanthoineCinelelocleFilms4 extends Migration
{
    public function up()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->renameColumn('lage_legal', 'age_legal');
        });
    }
    
    public function down()
    {
        Schema::table('maicanthoine_cinelelocle_films', function($table)
        {
            $table->renameColumn('age_legal', 'lage_legal');
        });
    }
}
