<?php namespace MaicAnthoine\Cinelelocle\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMaicanthoineCinelelocleVideos extends Migration
{
    public function up()
    {
        Schema::create('maicanthoine_cinelelocle_videos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('url', 100);
			$table->integer('id_film')->unsigned();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('maicanthoine_cinelelocle_videos');
    }
}
